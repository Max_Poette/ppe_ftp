﻿<?php

    if(isset($_SESSION['login']))
        { $action = "deconnexion"; }
        
    else
        { $action = "connexion"; }
        
    if(isset($_GET['action']))
        { $action = $_GET['action']; }
        
    switch($action)
    {
        case 'deconnexion' :
            // ON DECONNECTE L'UTILISATEUR
            unset($_SESSION['login']);
            echo '<meta http-equiv="refresh" content="0; url=index.php"/>';
            break;
        case 'connexion' :
            // AFFICHAGE DE LA PAGE CONNEXION
            include('vues/v_connexionForm.php');
            break;
        case 'validConnexion' :
            // VERIFICATION USER + REDIRECTION
            if(isset($_POST['login']) && isset($_POST['pass']))
                $pdo->verifierUser($_POST['login'], $_POST['pass']);
            echo isset($_SESSION['login']) ?
                '<meta http-equiv="refresh" content="0; url=index.php?uc=evenements&action=watchEvents"/>' :
                    '<meta http-equiv="refresh" content="0; url=index.php"/>';
            break;
        default :
            // DECONNEXION
            break;
    }
?>