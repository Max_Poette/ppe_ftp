<html>
    <head>
        <link rel="stylesheet" type="text/css" href="util/cssGeneral.css">
        <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="font-awesome/css/font-awesome.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="container">
            <!-- Features Section -->
            <div class="row">
                <h2 class="page-header">Liste des evenements</h2>
                <br>
                <div id="table" class="table-editable">
                        <div class="panel-group">
                            <div class="panel panel-primary ">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                                        Evenement [1]
                                    </a>
                                    </h4>
                                </div>
                                <br>
                                <br>
                                <center>
                                <div class="panel-body">
                                    <i style="font-size:1.8em;" class="glyphicon glyphicon-map-marker"></i>
                                    <h4>Bar-le-duc Place du lycée</h4>
                                    <h3 class="page-header"></h3>
                                        <br>
                                        <br>
                                    <i style="font-size:1.8em;" class="glyphicon glyphicon-calendar"></i>
                                    <h4>Jeudi 21 Decembre 2016</h4> 
                                    <h3 class="page-header"></h3>
                                        <br>
                                        <br>
                                    <i style="font-size:1.8em;" class="glyphicon glyphicon-time"></i>
                                    <h4>17h00</h4>
                                    <h3 class="page-header"></h3>
                                        <br>
                                        <br>
                                    <i style="font-size:1.8em;" class="glyphicon glyphicon-list-alt"></i>
                                    <h4>Reunion</h4>
                                    <h3 class="page-header"></h3>
                                        <br>
                                        <br>
                                    <i style="font-size:1.8em;" class="glyphicon glyphicon-tags"></i>
                                    <h4>Brosser sont chien</h4>
                                    <h3 class="page-header"></h3>
                                        <br>
                                        <br>
                                        <h2 class="page-header">Invités</h2>
                                        <br>
                                        <table>
                                            <tr>
                                                <td>
                                                    <i style="font-size:3em;" class="glyphicon glyphicon-user"></i>
                                                    <br>
                                                    Patrick
                                                </td>
                                                <td style="padding-left: 20px">
                                                    <i style="font-size:3em;" class="glyphicon glyphicon-user"></i>
                                                    <br>
                                                    Simon
                                                </td>
                                                <td style="padding-left: 20px">
                                                    <i style="font-size:3em;" class="glyphicon glyphicon-user"></i>
                                                    <br>
                                                    Josiane
                                                </td>
                                            </tr>
                                        </table>
                                        </center>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>   
    </body>
</html>
    



