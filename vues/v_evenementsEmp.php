<h2> Vos evenements à venir </h2>
<hr />

<?php
    foreach($lesEvenements as $e)
    {
        $lesGens = $pdo->getPeopleEvent($e['idE']);
        echo '
            <div id="table" class="table-editable" style = "width : 80%; margin:auto;">
                <div class="panel-group">
                    <div class="panel panel-primary ">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                '.$e['Description'].' à '.$e['lieu'].' ( '.$e['dateE'].' à '.$e['heureE'].' )
                            </h4>
                        </div>
                        <center>
                            <div class="panel-body">
                                <table>
                                    <tr>
        ';
                                        foreach($lesGens as $g)
                                        {
                                            echo '
                                                <td style = "padding : 2px;"><span style="font-size:2em;" class="glyphicon glyphicon-user"></span><br />'.$g[0].'</td>
                                            ';
                                        }
        echo '
                                    </tr>
                                </table>
                            </div>
                        </center>
                    </div>
                </div>
            </div>
        ';
    }
?>