<?php
    session_start();
    require_once("util/class.pdoGSB.inc.php");
    
    $uc = (!isset($_GET['uc'])) ? 'accueil' : $_GET['uc'];
    
    require("vues/v_head.php");
    if(!isset($_SESSION['login']))
        { $uc = 'connexion'; }
    else 
        { require("vues/v_banner.php"); }
    $pdo = PdoGSB::getPdoGSB();
    switch($uc)
    {
        case 'connexion' :
            { include("controleurs/c_connexion.php"); break; }
        case 'evenements' :
            { include("controleurs/c_evenements.php"); break; }
    }
?>
    </div>
</div>
<br/>
