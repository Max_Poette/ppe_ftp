﻿<?php
/** 
 * Classe d'accès aux données. 
 
 * Utilise les services de la classe PDO
 * pour l'application lafleur
 * Les attributs sont tous statiques,
 * les 4 premiers pour la connexion
 * $monPdo de type PDO 
 * $monPdoGsb qui contiendra l'unique instance de la classe
 *
 * @package default
 * @author Patrice Grand
 * @version    1.0
 * @link       http://www.php.net/manual/fr/book.pdo.php
 */

class PdoGSB
{   		
    private static $serveur='mysql:host=127.0.0.1';
    private static $bdd='dbname=gsb_ppe2016';
    private static $user='root' ;    		
    private static $mdp='' ;	
    private static $monPdo;
    private static $monPdoGSB = null;
/**
 * Constructeur privé, crée l'instance de PDO qui sera sollicitée
 * pour toutes les méthodes de la classe
 */				
	private function __construct()
	{
		PdoGSB::$monPdo = new PDO(PdoGSB::$serveur.';'.PdoGSB::$bdd, PdoGSB::$user, PdoGSB::$mdp);
		PdoGSB::$monPdo->query("SET CHARACTER SET utf8");
	}
	public function _destruct(){
		PdoGSB::$monPdo = null;
	}
/**
 * Fonction statique qui crée l'unique instance de la classe
 *
 * Appel : PdoGSB = PdoGSB::getPdoGSB();
 * @return l'unique objet de la classe PdoGSB
 */
    public static function getPdoGSB()
    {
        if(PdoGSB::$monPdoGSB == null)
        {
                PdoGSB::$monPdoGSB= new PdoGSB();
        }
        return PdoGSB::$monPdoGSB;
    }
    public function verifierUser($l, $m)
    {
        $req = "select count(*) as nb from employes where login='".$l."' and mdp='".$m."'";
        $res = PdoGSB::$monPdo->query($req);
        $LaLigne = $res->fetch();
        $nombre = $LaLigne['nb'];
        if ($nombre == 1)
        {
            $_SESSION["login"] = $l;
            return true;
        }
        return false;
    }

    public function getLesEvenements()
    {
        $lesEvenements=array();
        $req = "select * from evenement";
        $res = PdoGSB::$monPdo->query($req);
        $unEvenement = $res->fetchAll();
        $lesEvenements[] = $unEvenement;
        return $lesEvenements;
    }

    public function rechercher($b){
        $lesEmployes=array();
        $req = "select * from employes where nom like '".$b."%'";
        $res = PdoGSB::$monPdo->query($req);
        $unEmploye = $res->fetchAll();
        $lesEmployes[] = $unEmploye;
        return $lesEmployes;
    }

    public function employe()
    {
        $lesEmployes=array();
        $req = "select * from employes";
        $res = PdoGSB::$monPdo->query($req);
        $unEmploye = $res->fetchAll();
        $lesEmployes[] = $unEmploye;
        return $lesEmployes;
    }

    public function getInfo($id){
        $lesInfos=array();
        $req = "select * from employes where idEm = ".$id;
        $res = PdoGSB::$monPdo->query($req);
        $uneInfo = $res->fetch();
        $lesInfos[] = $uneInfo;
        return $lesInfos;
    }

    public function getEventPeople($id){
        $req = "select * from evenement inner join convoquer on evenement.idE=convoquer.idE left join employes on convoquer.idE=employes.idEm where convoquer.idEm = ".$id;
        $res = PdoGSB::$monPdo->query($req);
        return $res->fetchAll();
    }
    public function getPeopleEvent($idEvent) 
    {
        $req = "select employes.nom, employes.prenom from evenement inner join convoquer on evenement.idE=convoquer.idE left join employes on convoquer.idEm=employes.idEm where convoquer.idE = ".$idEvent;
        $res = PdoGSB::$monPdo->query($req);
        $unGens = $res->fetchAll();
        return $unGens;
    }
    public function getServicePeople($id){
            $req = "select libelle from service inner join employes on employes.idS=service.idS  where employes.idEm = ".$id;
            $res = PdoGSB::$monPdo->query($req);
            $libel = $res->fetchAll();
            return $libel[0];
    }
    public function getEmployeById($id){
            $req = "select * from employes where idEm = '".$id."'";
            $res = PdoGSB::$monPdo->query($req);
            $resul = $res->fetchAll();
            return $resul;
    }
    public function getIdEmploye($nom){
            $req = "select idEm from employes where login = '".$nom."'";
            $res = PdoGSB::$monPdo->query($req);
            $resul = $res->fetch();
            return $resul[0];
    }

    public function Ajouterevent($id, $event){
            $req = "insert into participe values (".$id.", ".$event.")";
            $res = PdoGSB::$monPdo->query($req);
    }

    public function Deleteevent($id, $event){
            $req = "delete from participe where idE = ".$event." and idEm = ".$id;
            $res = PdoGSB::$monPdo->query($req);
    }

    public function getInfosEvent($info)
    {
            $lesInfos=array();
            $req = "select * from evenement where idE =".$info;
            //echo $req;
            $res = PdoGSB::$monPdo->query($req);
            $infoEvent = $res->fetchAll();
            $lesInfos[] = $infoEvent;
            //echo var_dump($lesInfos);
            return $lesInfos;
    }

    public function getEmployesEvent($idevent)
    {
        $lesEmployes=array();
        $req = "select nom from employes inner join convoquer on employes.idEm=convoquer.idEm where convoquer.idE = ".$idevent;
        //echo "<br/>".$req;
        $res = PdoGSB::$monPdo->query($req);
        $Employe = $res->fetchAll();
        $lesEmployes[] = $Employe;
        return $lesEmployes;
    }

    public function ValidModif($id, $inti, $lieu, $date, $heure)
    {
        $req = "UPDATE evenement SET lieu='".$lieu."', dateE='".$date."', heureE='".$heure."', Description='".$inti."' WHERE idE=".$id;
        $res = PdoGSB::$monPdo->query($req);
    }

    public function SupprEvent($id)
    {
        $req = "DELETE from evenement WHERE idE=".$id;
        $res = PdoGSB::$monPdo->query($req);
    }

    public function AddEvent($inti, $lieu, $date, $heure)
    {
        $req = "select max(idE) from evenement";
        $res = PdoGSB::$monPdo->query($req);
        $id = $res->fetchAll();
        $id[0][0]++;
        $req = "INSERT INTO evenement VALUES (".$id[0][0].",'".$lieu."','".$date."','".$heure."','".$inti."')";
        echo $req;
        $res = PdoGSB::$monPdo->query($req);
    }

    public function ScriptMail()
    {
        $date = date("Y")."-".date("m")."-".(date("d")+2);
        $req = "select * from evenement where dateE < '".$date."'";
        $res = PdoGSB::$monPdo->query($req);
        $event = $res->fetchAll();
        $lesEvenements[] = $event;
        foreach ($lesEvenements[0] as $c){
            $message = "N'oubliez pas votre evenement: ".$c["Description"]." du : ".$c["dateE"].". Lieu : ".$c["lieu"].". Heure : ".$c["heureE"].". On compte sur vous !";
            $message = wordwrap($message, 70, "\r\n");
            $req = "select login from employes inner join convoquer on employes.idEm=convoquer.idEm where idE = ".$c['idE'];
            var_dump($req);
            $res = PdoGSB::$monPdo->query($req);
            if($res){
                $mail = $res->fetchAll();
                $lesMails[] = $mail;
                foreach ($lesMails[0] as $m){
                    $email = $m["login"];
                    mail($email, 'Réunion', $message);
                }
            }
        }
    }
}
?>